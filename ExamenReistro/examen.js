
const conexion = require('./cone_base_datos')

module.exports = app => {
    const connect = conexion

    app.post('/registro_datos', (req, res) => {
        const cedula_c = req.body.cedula
        const apellidos_c = req.body.apellidos
        const nombres_c = req.body.nombres
        const direccion_c = req.body.direccion
        const telefono_c = req.body.telefono

        connect.query('insert into clientes SET ?', {
            cedula_c, apellidos_c, nombres_c, direccion_c, telefono_c
        }, (error, resultado) => {
            res.redirect('/registro')
        })
    })
}
